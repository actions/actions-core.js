var extend = require('util')._extend;
var Promise = require('promise');
var tea = require('tea-properties');

var Core = function() {};

Core.prototype.has = function(name) {
  return (this.get(name) !== undefined);
};

Core.prototype.get = function(name) {
  if (this[name]) {
    return this[name];
  }

  // this function has to be replaced to improve performance
  return tea.get(this, name);
};

Core.prototype.add = function(name, functions) {
  if (this[name]) {
    return extend(this[name], functions);
  }
  return tea.set(this, name, functions);
};

Core.prototype.run = function(action, callback) {
  var self = this;
  var solve = function(resolve) {
    var fn = self.get(action.name);
    // If function is asynchronous
    if (fn.async) {
      return fn(action.args, resolve);
    } else {
      return resolve(fn(action.args));
    }
  };

  if (callback) {
    return solve(callback);
  }

  return new Promise(solve);
};

module.exports = Core;
