
var html = {};

var title = {name: 'title', selector: 'title'};
var links = {name: 'links', selector: ['a']};

[title].forEach(function(action) {
	var selectors = {};
	selectors[action.name] = action.selector;
	html[action.name] = {
		name: 'html.extract',
		args: {
			selectors: selectors
		}
	};
});

module.exports = html;
