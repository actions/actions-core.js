var cheerio = require('cheerio');

var html = module.exports = {};

html.extract = function(context) {
  if (!context.selectors || context.selectors.constructor !== Object) {
    return context;
  }

  var $ = cheerio.load(context.body);

  var results = {};
  var selectorNames = Object.getOwnPropertyNames(context.selectors);
  for (var i = selectorNames.length - 1; i >= 0; i--) {
    var selectorName = selectorNames[i];
    var selector = context.selectors[selectorName];
    var extract = 'text';

    if (selector.constructor == Object) {
      if (selector.extract) {
        extract = selector.extract;
      }
      selector = selector.path;
    }

    var node = $(selector);

    var result;
    if (extract == 'text') {
      result = node.text();
    } else {
      result = node.attr(extract);
    }

    results[selectorName] = result;
  }

  return results;
};
