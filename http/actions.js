
/*
* 
* shortcuts
* * http.get
* * http.post
* * http.put
* * http.delete
* 
*/

var http = module.exports = {};

['GET', 'POST', 'PUT', 'DELETE'].forEach(function(method) {
  var lower = method.toLocaleLowerCase();
  var upper = method.toLocaleUpperCase();
  http[lower] = {
    name: 'http.request',
    args: {
      method: upper
    }
  };
});
