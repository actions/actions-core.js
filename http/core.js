var request = require('request');

var http = module.exports = {};

http.request = function(context, callback) {
  var method = context.method || 'GET';
  var args = {
    method: method,
    url: context.url,
  };
  request(args, function(err, response, body) {
    callback({
      status: response.statusCode,
      body: body,
      url: context.url
    });
  });
};

http.request.async = true;
