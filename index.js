var Core = require('./core');

var actions = {};
var core = new Core();

['html', 'http'].forEach(function(name) {
	// Add actions
	actions[name] = require('./' + name + '/actions');
	
	// and core functions
	core.add(name, require('./' + name + '/core'));
});

module.exports = {
	actions: actions,
	core: core
};
